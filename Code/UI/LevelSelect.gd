extends PanelContainer
class_name LevelSelect

@onready var grass: Button = $VBox/HBox/Grass
@onready var snow = $VBox/HBox/Snow
@onready var lava = $VBox/BottomHBox/Lava
@onready var nothing = $VBox/BottomHBox/Nothing
@onready var desert = $VBox/BottomHBox/Desert


func _ready():
	grass.pressed.connect(OnGrassPressed)
	snow.pressed.connect(OnSnowPressed)
	lava.pressed.connect(OnLavaPressed)
	nothing.pressed.connect(OnNothingPressed)
	desert.pressed.connect(OnDesertPressed)
	show()
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func OnGrassPressed():
	print("L bozo")
	get_tree().change_scene_to_file("res://Levels/Lv_3.tscn")

func OnSnowPressed():
	print("Die Die Die")
	get_tree().change_scene_to_file("res://Levels/Lv_4.tscn")

func OnLavaPressed():
	print("Hu HU HUU..Cough Cough")
	get_tree().change_scene_to_file("res://Levels/Lv_1.tscn")

func OnNothingPressed():
	print("HMM, Hmm Hmmmmmm!")
	get_tree().change_scene_to_file("res://Levels/Nothing Map.tscn")

func OnDesertPressed():
	print("You are dying.")
	get_tree().change_scene_to_file("res://Levels/Lv_2.tscn")
