extends PanelContainer

@onready var play_button = $VBox/PlayButton


# Called when the node enters the scene tree for the first time.
func _ready():
	play_button.pressed.connect(OnPlayPressed)
	show()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func OnPlayPressed():
	hide()
