extends Node
class_name Main

@onready var button = $Button

func _ready():
	button.pressed.connect(OnButtonPressed)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func OnButtonPressed():
	get_tree().change_scene_to_file("res://Levels/Lv_3.tscn")

