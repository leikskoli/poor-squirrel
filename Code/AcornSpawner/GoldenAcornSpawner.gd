extends Node2D
class_name GoldenAcornSpawner
const ACORN = preload("res://Acorn/Goldilocks-the-acorn.tscn")
@onready var timer = $Timer

func _ready():
	timer.timeout.connect(SpawnAcorn)

func SpawnAcorn():
	print("spawningacorn")
	var acorn = ACORN.instantiate()
	acorn.position = position
	get_parent().add_child(acorn)
